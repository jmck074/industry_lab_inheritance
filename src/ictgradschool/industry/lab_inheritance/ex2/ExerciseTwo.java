package ictgradschool.industry.lab_inheritance.ex2;

/**
 * Main program for Exercise Two.
 */
public class ExerciseTwo {

    public void start() {

        IAnimal[] animals = new IAnimal[3];

        // TODO Populate the animals array with a Bird, a Dog and a Horse.
        animals[0] = new Bird();
        animals[1] = new Dog();
        animals[2] = new Horse();

        processAnimalDetails(animals);

    }

    private void processAnimalDetails(IAnimal[] list) {
        // TODO Loop through all the animals in the given list, and print their details as shown in the lab handout.
        for (int i = 0; i < list.length; i++) {
            System.out.println(list[i].myName() + " " + list[i].sayHello() + ".");
            if (list[i].isMammal()) {
                System.out.println(list[i].myName() + " is a mammal.");
            } else {
                System.out.println(list[i].myName() + " is a non-mammal.");
            }

            System.out.println("Did I forget to tell you that I have " + list[i].legCount() + " legs.");
            if (list[i] instanceof IFamous) {
                IFamous h = (IFamous) list[i];
                //IFamous d = (Dog) list[i];
                System.out.println("This is a famous name of my animal type: " + h.famous());
                //System.out.println("This is a famous name of my animal type: " + d.famous());
            }


            System.out.println("----------------------------------------------");
        }


        // TODO If the animal also implements IFamous, print out that corresponding info too.


    }

    public static void main(String[] args) {
        new ExerciseTwo().start();

        SuperClass s2 = new Test1();
        System.out.println("\nThe static Binding");
        System.out.println("S2.x = " + s2.x);
        System.out.println("S2.y = " + s2.y);
        System.out.println("S2.foo() = " + s2.foo());
        System.out.println("S2.goo2() = " + s2.goo());
    }
}
